import json
from flask import request, Response, Blueprint
import sqlalchemy
from connection import Connection
from instance import Instance

bp_conn = Blueprint('api_connection', __name__)

def construct_headers_location_url(request, conn):
    ip = Instance.get_ip(request)
    port = Instance.get_port(request)
    conn_id = conn.get_instanceID()
    location_url = f"https://{ip}:{port}/connections/{conn_id}"
    return location_url

@bp_conn.route('/', methods=['POST'])
def connect_instance():
    conn = Connection(request.args.get('instance_name'), request)
    try:
        conn.add_connection()
        location_url = construct_headers_location_url(request, conn)
        headers = {"Location" : f"{location_url}"}
        
        return Response(response=json.dumps({'Connection' : Connection.json(conn)},
                        indent=4, sort_keys=True, default=str),
                        status=201,
                        headers=headers,
                        mimetype='application/json')

    except sqlalchemy.exc.IntegrityError:
        return Response("Connection with that name already exists",
                        status=400,
                        mimetype='application/json')

@bp_conn.route('/<int:_id>', methods=['GET'])
def get_connection_by_id(_id):
    try:
        user = Connection.get_connection_by_id(_id)
        
        return Response(response=json.dumps({'User ' : Connection.json(user)}),
                        status=200,
                        mimetype='application/json')
    except AttributeError:
        return Response("No connection with that ID exists.",
                        status=404,
                        mimetype='application/json')

@bp_conn.route('/all', methods=['GET'])
def get_all_connected_instances():
    results = Connection.get_all_instances()
    if results != []:
        new_res = [Connection.json(r) for r in results]

        return Response(response=json.dumps({'Connected Instances ' : new_res}),
                        status=200,
                        mimetype='application/json')

    return Response("No instances found",
                    status=404,
                    mimetype='application/json')

@bp_conn.route('/count', methods=['GET'])
def connections_count():
    number = Connection.get_conn_instances_count()

    return Response(response=json.dumps({'Number of Connections ' : number}),
                    status=200,
                    mimetype='application/json')

@bp_conn.route('/<int:_id>', methods=['DELETE'])
def remove_connection(_id):
    Connection.remove_connection(_id)

    return Response(status=204,
                    mimetype='application/json')
