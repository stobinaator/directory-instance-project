import unittest
import requests
from instance import Instance
from connection import Connection 

class TestInstance(unittest.TestCase):
    API_INST1_URL = "http://127.0.0.1:5001"
    API_INST2_URL = "http://127.0.0.1:5002"
    API_INST3_URL = "http://127.0.0.1:5003"
    # connection urls
    CONNS_POST_URL = '/connections'
    # authentication urls
    AUTH_POST_URL = '/authentications'
    # /files - /single & /batch
    INST_S_UPLOAD_URL = '/instances/files/single'
    INST_B_UPLOAD_URL = '/instances/files/batch'
    # /count 
    INST_GET_COUNT_URL = '/instances/count'
    # /all GET
    INST_GET_ALL_URL = '/instances/all'
    # DELETE
    INST_DELETE_URL = '/instances/'
    # /files - searching
    INST_SEARCH_URL = '/instances/files'
    

    @unittest.SkipTest
    def test_1_upload_files_count(self):
        params = {'instance_name' : 'KIT1'}
        params2 = {'instance_name' : 'KIT2'}
        instance1_url = TestInstance.API_INST1_URL + TestInstance.CONNS_POST_URL
        instance2_url = TestInstance.API_INST2_URL + TestInstance.CONNS_POST_URL
        
        r = requests.post(instance1_url, params=params)
        self.assertTrue(r.status_code, 201)
        self.assertTrue(r.content, 'Connection added successfully') 

        r2 = requests.post(instance2_url, params=params2)
        self.assertTrue(r2.status_code, 201)
        self.assertTrue(r2.content, 'Connection added successfully')
        
        instance1_upload_ulr = TestInstance.API_INST1_URL + TestInstance.INST_B_UPLOAD_URL
        
        directory_path = "/Users/stobko/Downloads/newest_shit/files"
        r3 =  requests.post(instance1_upload_ulr, json={"directory_path": f"{directory_path}"}) 
        self.assertTrue(r3.status_code, 201)
        self.assertTrue(r3.content, 'Batch Files added to instance successfully')

        instance1_count = TestInstance.API_INST1_URL + TestInstance.INST_GET_COUNT_URL
        instance2_count = TestInstance.API_INST2_URL + TestInstance.INST_GET_COUNT_URL
        r4 = requests.get(instance1_count)
        self.assertTrue(r4.status_code, 200)
        self.assertTrue(r4.content, 4)

        r5 = requests.get(instance2_count)
        self.assertTrue(r5.status_code, 200)
        self.assertTrue(r5.content, 0)

    @unittest.SkipTest
    def test_x_add_user(self):
        params = {'user_name' : 'Stoyan'}
        instance1_auth_url = TestInstance.API_INST1_URL + TestInstance.AUTH_POST_URL
        
        r = requests.post(instance1_auth_url, params=params)
        self.assertTrue(r.status_code, 201)
        self.assertTrue(r.content, 'User added successfully')
        
        # instance1_search_url = TestInstance.API_INST1_URL + INST_SEARCH_URL
        # requests.get()

    @unittest.SkipTest
    def test_3_search_file_locally(self):
        instance1_search_url = TestInstance.API_INST1_URL + TestInstance.INST_SEARCH_URL
        headers = {"User-Token" : "LyNAvq9bvq7Zl-cBFSeX3zbPtmE"}
        params = {'file_name' : 'file'}
        
        r1 = requests.get(instance1_search_url, headers=headers, params=params)
        self.assertTrue(r1.status_code, 200)

if __name__ == '__main__':
    unittest.main()