import secrets
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, String, Integer

db = SQLAlchemy()

class Auth(db.Model):
    __bind_key__ = 'auth'
    __tablename__ = 'authenticated_users'
    id = Column(Integer, primary_key=True)
    user_name = Column(String(30), unique=True, nullable=False)
    user_token = Column(String(27), unique=True, nullable=False)

    def __init__(self, user_name):
        self.user_name = user_name
        self.user_token = secrets.token_urlsafe(20)

    def add_user(self):
        db.session.add(self)
        db.session.commit()

    def get_userID(self):
        return self.id

    def get_user_token(self):
        return self.user_token

    @classmethod
    def get_user_by_id(cls, _id):
        return db.session.query(Auth).filter_by(id=_id).first()

    @staticmethod
    def get_all_users():
        return db.session.query(Auth).all()

    @classmethod
    def remove_user(cls, _id):
        Auth.query.filter_by(id=_id).delete()
        db.session.commit()

    @classmethod
    def remove_all_users(cls):
        db.session.query(Auth).delete()
        db.session.commit()

    @classmethod
    def check_user_token_exists(cls, u_token):
        return bool(db.session.query(Auth).filter_by(user_token=u_token).first())

    @classmethod
    def get_user_count(cls):
        return db.session.query(Auth).count()

    def json(self):
        return {'id' : self.id,
                'user_name' : self.user_name}

    def __repr__(self):
        return '<User id:%r name:%r>' % (self.id, self.user_name)
