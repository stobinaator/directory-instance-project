import json
from datetime import datetime, timedelta
from functools import lru_cache, wraps
import requests
from flask import request, Response, Blueprint
from auth import Auth
from instance import Instance
from connection import Connection

bp_inst = Blueprint('api_instance', __name__)


def timed_lru_cache(seconds: int, maxsize: int = 5):
    def wrapper_cache(func):
        func = lru_cache(maxsize=maxsize)(func)
        func.lifetime = timedelta(seconds=seconds)
        func.expiration = datetime.utcnow() + func.lifetime

        @wraps(func)
        def wrapped_func(*args, **kwargs):
            if datetime.utcnow() >= func.expiration:
                func.cache_clear()
                func.expiration = datetime.utcnow() + func.lifetime
            return func(*args, **kwargs)

        return wrapped_func

    return wrapper_cache

def construct_headers_location_url(request, added_file):
    ip = Instance.get_ip(request)
    port = Instance.get_port(request)
    file_id = added_file.get_fileID()
    location_url = f"https://{ip}:{port}/instances/{file_id}"
    return location_url

@bp_inst.route('/files/single', methods=['POST'])
def upload_single_file():
    request_data = request.get_json()
    try:
        file_name = Instance.get_file_name(request_data['file_path'])
        if Instance.check_file_exists(file_name) is not True:
            i = Instance(file_name, request_data)
            i.add_file()
            location_url = construct_headers_location_url(request, i)
            headers = {"Location" : f"{location_url}"}

            return Response(response=json.dumps({'File' : Instance.json(i)},
                            indent=4, sort_keys=True, default=str),
                            status=201,
                            headers=headers,
                            mimetype='application/json')

        return Response('File with that name already exists',
                            status=400,
                            mimetype='application/json')
    except KeyError:
        return Response("Check you spelling. It should be 'file_path'.",
                        status=400,
                        mimetype='application/json')
    except FileNotFoundError:
        return Response("No file with that name could be found, to be uploaded.",
                        status=404,
                        mimetype='application/json')

@bp_inst.route('/files/batch', methods=['POST'])
def upload_batch_files():
    request_data = request.get_json()
    try:
        if Instance.add_multiple_files(request_data) != 0:
            return Response("Batch of files added to instance successfully",
                            status=201,
                            mimetype='application/json')

        return Response("All of the files already exist",
                        status=400,
                        mimetype='application/json')
    except KeyError:
        return Response("Check you spelling. It should be 'directory_path'.",
                        status=400,
                        mimetype='application/json')
    except FileNotFoundError:
        return Response("No directory with that name could be found, to be uploaded.",
                        status=404,
                        mimetype='application/json')

@bp_inst.route('/<int:_id>', methods=['GET'])
def get_file_by_id(_id):
    try:
        file = Instance.get_file_by_id(_id)
        
        return Response(response=json.dumps({'File ' : Instance.json(file)}, indent=4, 
                                            sort_keys=True, default=str),
                        status=200,
                        mimetype='application/json')
    except AttributeError:
        return Response("No file with that ID exists.",
                        status=404,
                        mimetype='application/json')

@bp_inst.route('/all', methods=['GET'])
@timed_lru_cache(60)
def get_all_instances():
    result = Connection.get_all_instances()
    if result == []:
        return Response('There are NO connected instances.',
                        status=404,
                        mimetype='application/json')

    new_list = [Connection.json(r) for r in result]
    return Response(response=json.dumps({'Connections' : new_list}),
                    status=200,
                    mimetype='application/json')

@bp_inst.route('/count', methods=['GET'])
def files_count():
    number = Instance.get_files_count()
    return Response(response=json.dumps({'Number of files ' : number}),
                    status=200,
                    mimetype='application/json')

def getting_instance_info(_request):
    instance_port = Instance.get_port(_request)
    instance_name = Connection.get_own_instance_name(instance_port)
    instance_token = Connection.get_own_instance_token(instance_port)
    return instance_port, instance_name, instance_token

@bp_inst.route('/files/<string:file_name>', methods=['GET'])
@timed_lru_cache(20)
def search_file(file_name):
    limit = request.args.get('limit')

    if request.headers.get('User-Token'):
        if Auth.check_user_token_exists(request.headers.get('User-Token')):
            if request.args.get('search_remote') == 'True':
                instance_port, instance_name, instance_token = getting_instance_info(request)
                _ips = Connection.get_ips()
                _ports = Connection.get_ports()

                Instance.create_inmem()

                for ip, port in zip(_ips, _ports):
                    # skip sending a request to the instance that received this request
                    if port != instance_port:
                        url = f'http://{ip}:{port}/instances/files/{file_name}'
                        headers = {"Instance-Token" : f"{instance_token}"}
                        requests.get(url, headers=headers)
                    else:
                        result = Instance.get_file_by_name(file_name)
                        new_list = [Instance.json(r) for r in result]
                        Instance.update_inmem(new_list, instance_name)

                files = Instance.query_inmem_for_files(file_name, limit)

                if files != []:
                    response = Response(response=json.dumps({'Found': files}),
                                        status=200,
                                        mimetype='application/json')
                else:
                    response = Response('No file with that name found!\n' \
                                        'Content is cached for XX seconds. Wait a little.',
                                        status=404,
                                        mimetype='application/json')
                return response
            else:
                # search_remote is None or False
                # search only in local database
                result = Instance.get_file_by_name(file_name, limit)
                if result == []:
                    response = Response('No file with that name found locally',
                                        status=404,
                                        mimetype='application/json')
                else:
                    new_list = [Instance.json(r) for r in result]
                    response = Response(response=json.dumps({"File(s): " : new_list}),
                                        status=200,
                                        mimetype='application/json')
                return response
        else:
            return Response('No user with that User-Token found',
                            status=404,
                            mimetype='application/json')

    elif request.headers.get('Instance-Token'):

        if Connection.check_instance_token_exists(request.headers.get('Instance-Token')):

            instance_name = Connection.get_own_instance_name(Instance.get_port(request))

            result = Instance.get_file_by_name(file_name)
            if result == []:
                response = Response(f'No file with that name found on {instance_name}',
                                    status=404,
                                    mimetype='application/json')
            else:
                new_list = [Instance.json(r) for r in result]
                Instance.update_inmem(new_list, instance_name)
                response = "done"
            return response

        return Response('No instance with that Instance-Token found!',
                        status=404,
                        mimetype='application/json')
    else:
        return Response("You are not authorised",
                        status=401,
                        mimetype='application/json')

@bp_inst.route('/<int:_id>', methods=['DELETE'])
def delete_file(_id):
    Instance.remove_file(_id)
    return Response('File deleted from instance',
                    status=204,
                    mimetype='application/json')

@bp_inst.route('/all', methods=['DELETE'])
def delete_all_files():
    Instance.remove_all_files()
    return Response('Multiple files deleted from instance',
                        status=204,
                        mimetype='application/json')
