import os
import sys
import pathlib


from flask import Flask
from flask_caching import Cache


app = Flask(__name__)
cache = Cache()

#config
app.config.from_object('config.DevelopmentConfig')

path = os.path.join(pathlib.Path(__file__).parent.absolute(), "dbs/")
# it cant be read from the config object. WHY?
app.config['SQLALCHEMY_BINDS'] = {'connection' : f'sqlite:///{path}/connection.db',
                                    'auth' : f'sqlite:///{path}/auth.db'}
app.config['CACHE_TYPE'] = 'simple'
app.url_map.strict_slashes = False

with app.app_context():
    from instance import db as db1
    from connection import db as db2
    from auth import db as db3

    db1.init_app(app)
    db1.create_all()
    db2.create_all()
    db3.create_all()

    from api_connection import bp_conn
    from api_instance import bp_inst
    from api_authentication import bp_auth


    app.register_blueprint(bp_conn, url_prefix='/connections')
    app.register_blueprint(bp_inst, url_prefix='/instances')
    app.register_blueprint(bp_auth, url_prefix='/authentications')

if __name__ == '__main__':
    app.run(port=sys.argv[2], debug=True)
