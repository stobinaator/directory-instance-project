import secrets
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, String, Integer

db = SQLAlchemy()

class Connection(db.Model):
    __bind_key__ = 'connection'
    __tablename__ = 'connected_instances'
    id = Column(Integer, primary_key=True)
    instance_name = Column(String(50), unique=True, nullable=False)
    instance_ip = Column(String(50), nullable=False)
    instance_port = Column(String(4), nullable=False)
    instance_token = Column(String(27), unique=True, nullable=False)

    def __init__(self, instance_name, request):
        instance_ip, instance_port = Connection.deconstruct_request(request)
        self.instance_name = instance_name
        self.instance_ip = instance_ip
        self.instance_port = instance_port
        self.instance_token = secrets.token_urlsafe(20)
    
    @classmethod
    def deconstruct_request(cls, request):
        """Getting the ip and port of the calling instance 
            from the request. 
        :return: The instance_ip and _port.
        """
        split_request = request.host_url.split('/')
        second_split = split_request[2].split(':')
        ip = second_split[0]
        port = second_split[1]
        return ip, port

    def add_connection(self):
        db.session.add(self)
        db.session.commit()

    def get_instance_token(self):
        return self.instance_token

    def get_instance_name(self):
        return self.instance_name
    
    def get_instanceID(self):
        return self.id

    @classmethod
    def get_connection_by_id(cls, _id):
        return db.session.query(Connection).filter_by(id=_id).first()

    @staticmethod
    def get_all_instances():
        return db.session.query(Connection).all()

    @classmethod
    def get_conn_instances_count(cls):
        return db.session.query(Connection).count()

    @staticmethod
    def remove_connection(_id):
        Connection.query.filter_by(id=_id).delete()
        db.session.commit()

    @staticmethod
    def check_instance_token_exists(i_token):
        return bool(db.session.query(Connection).filter_by(instance_token=i_token).first())

    @classmethod
    def get_own_instance_name(cls, inst_port):
        instance = db.session.query(Connection).filter_by(instance_port=inst_port).first()
        return instance.get_instance_name()

    @classmethod
    def get_own_instance_token(cls, inst_port):
        instance = db.session.query(Connection).filter_by(instance_port=inst_port).first()
        return instance.get_instance_token()

    @classmethod
    def get_ips(cls):
        """Get all the ips of every connected instance,
            meaning each one that can be found in the "phonebook"
            Getting the "clean version" of the query, without the brackets.
        :return: Found IPs 
        """
        ips = db.session.query(Connection.instance_ip).all()
        clean_ips = [ip[0] for ip in ips]
        return clean_ips

    @classmethod
    def get_ports(cls):
        """Get all the port of every connected instance,
            meaning each one that can be found in the "phonebook"
            Getting the "clean version" of the query, without the brackets.
        :return: Found Ports 
        """
        ports = db.session.query(Connection.instance_port).all()
        clean_ports = [port[0] for port in ports]
        return clean_ports

    def json(self):
        return {'id' : self.id,
                "instance_name" : self.instance_name,
                "instance_ip" : self.instance_ip,
                "instance_port" : self.instance_port}

    def __repr__(self):
        return '<Connection id:%r name:%r ip:%r port:%r>' % \
                (self.id, self.instance_name, self.instance_ip, self.instance_port)
