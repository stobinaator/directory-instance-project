import json
from flask import request, Response, Blueprint
import sqlalchemy
from auth import Auth
from instance import Instance

bp_auth = Blueprint('api_authentication', __name__)

def construct_headers_location_url(request, auth_user):
    ip = Instance.get_ip(request)
    port = Instance.get_port(request)
    user_id = auth_user.get_userID()
    location_url = f"https://{ip}:{port}/authentications/{user_id}"
    return location_url

@bp_auth.route('/', methods=['POST'])
def authenticate_user():
    authentication = Auth(request.args.get('user_name'))
    
    try:
        authentication.add_user()
        location_url = construct_headers_location_url(request, authentication)
        headers = {"Location" : f"{location_url}"}

        return Response(response=json.dumps(
                                {'User' : Auth.json(authentication),
                                'User Token' : f"{authentication.get_user_token()}"},
                        indent=4, sort_keys=True, default=str),
                        status=201,
                        headers=headers,
                        mimetype='application/json')

    except sqlalchemy.exc.IntegrityError:
        return Response("User with that name already exists",
                        status=400,
                        mimetype='application/json')

@bp_auth.route('/all', methods=['GET'])
def get_all_users():
    results = Auth.get_all_users()
    if results != []:
        new_res = [Auth.json(r) for r in results]

        return Response(response=json.dumps({'Users ' : new_res}),
                        status=200,
                        mimetype='application/json')

    return Response("No authenticated users found",
                    status=404,
                    mimetype='application/json')

@bp_auth.route('/<int:_id>', methods=['GET'])
def get_user_by_id(_id):
    try:
        user = Auth.get_user_by_id(_id)

        return Response(response=json.dumps({'User ' : Auth.json(user)}),
                        status=200,
                        mimetype='application/json')
    except AttributeError:
        return Response("No user with that ID exists.",
                        status=404,
                        mimetype='application/json')
    
@bp_auth.route('/count', methods=['GET'])
def user_count():
    number = Auth.get_user_count()

    return Response(response=json.dumps({'Connected Users ' : number}),
                    status=200,
                    mimetype='application/json')
                    
@bp_auth.route('/<int:_id>', methods=['DELETE'])
def delete_user(_id):
    Auth.remove_user(_id)

    return Response(status=204,
                    mimetype='application/json')

@bp_auth.route('/all', methods=['DELETE'])
def delete_all_users():
    Auth.remove_all_users()

    return Response(status=204,
                    mimetype='application/json')
