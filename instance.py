import sqlite3
import os
import pathlib
import datetime
import math
import magic
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, String, Integer, Float, DateTime, LargeBinary

db = SQLAlchemy()

class Instance(db.Model):
    __tablename__ = "uploaded_files"
    id = Column(Integer, primary_key=True)
    file_name = Column(String(50), unique=True, nullable=False)
    file_type = Column(String(15), nullable=False)
    file_path = Column(String(100), unique=True, nullable=False)
    file_size = Column(Float, nullable=False)
    file_size_unit = Column(String(10), nullable=False)
    last_modified = Column(DateTime, nullable=False)
    file = Column(LargeBinary)

    def __init__(self, file_name, request_data, whole_path=None):
        if whole_path is None:
            f_read = pathlib.Path(request_data['file_path'])
            size, unit = self.round_up_file_size(os.path.getsize(f_read), 2)
            self.file_name = file_name
            self.file_type = self.get_file_type_and_replace(request_data['file_path'])
            self.file_path = request_data['file_path']
            self.file_size = size
            self.file_size_unit = unit
            self.last_modified = datetime.datetime.fromtimestamp(f_read.stat().st_mtime)
            self.file = self.convert_file(request_data['file_path'])
        else:
            f_read = pathlib.Path(whole_path)
            size, unit = self.round_up_file_size(os.path.getsize(f_read), 2)
            self.file_name = file_name
            self.file_type = self.get_file_type_and_replace(whole_path)
            self.file_path = whole_path
            self.file_size = size
            self.file_size_unit = unit
            self.last_modified = datetime.datetime.fromtimestamp(f_read.stat().st_mtime)
            self.file = self.convert_file(whole_path)

    def add_file(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def add_multiple_files(cls, request_data):
        count = 0
        directory = request_data['directory_path']
        files_list = Instance.clean_files_list(directory)

        for file_n in files_list:
            whole_path = os.path.join(directory, file_n)
            file_n = file_n.split('.',1)

            if Instance.check_file_exists(file_n[0]) is not True:
                i = Instance(file_n[0], request_data, whole_path)
                db.session.add(i)
                count += 1

            continue
        db.session.commit()
        return count

    @classmethod
    def clean_files_list(cls, directory):
        """Sometimes when uploading a whole directory of files, the file
            ".DS_Store" (on OSx) can be found and also uploaded.
        :return: Files list cleaned up.
        """
        # sometimes by batch uploads a .DS_Store can be found on osX
        # manually removing it from the list with files
        files_list = []
        for d in os.listdir(directory):
            if d == '.DS_Store':
                continue
            files_list.append(d)
        return files_list

    def get_file_type_and_replace(self, file_path):
        """Normally file_types are in a format splitted
            with a forward slash like here "image/png".
            This function changes the "/" into a dash "-".   
        :return: File type adjusted.
        """
        f_type = magic.from_file(file_path, mime=True)
        f_type = f_type.replace('/','-')
        return f_type

    def convert_file(self, path):
        filename = path
        with open(filename, 'rb') as f:
            file = f.read()
        return file

    def round_up_file_size(self, n, decimals=0):
        """Rounding up the size of the file, because the 'n' argument
            is just a large number. This function tries to show the thousands 
            or millions of bytes the file is.
        :return: Size of file as in integer and additionally
                a unit string.
        """
        if len(str(n)) < 3:
            multiplier = 10 ** decimals
            result = math.ceil(n * multiplier) / multiplier
            unit = 'bytes'
            return result, unit

        elif len(str(n)) > 3 and len(str(n)) < 7:
            n = n / 1000
            multiplier = 10 ** decimals
            result = math.ceil(n * multiplier) / multiplier
            unit = 'kBytes'
            return result, unit

        elif len(str(n)) >= 7:
            n = n / 1000000
            multiplier = 10 ** decimals
            result = math.ceil(n * multiplier) / multiplier
            unit = 'mBytes'
            return result, unit
        
    def get_fileID(self):
        return self.id

    @classmethod
    def get_file_name(cls, file_path):
        """Getting the file's name of the file that is to be uploaded,
            without the extension. From the url that the user provides.
            /Users/me/Downloads/files/file.txt -> onyl "file"
        :return: File name
        """
        words_list = file_path.split('/')
        words_list = words_list[-1].split('.')
        return words_list[0]

    @classmethod
    def get_file_by_id(cls, _id):
        return db.session.query(Instance).filter_by(id=_id).first()

    @classmethod
    def get_file_by_name(cls, file_name, limit=None):
        if limit is None:
            return db.session.query(Instance).\
                    filter(Instance.file_name.ilike('%' + file_name +'%')).all()
        return db.session.query(Instance).\
                filter(Instance.file_name.ilike('%' + file_name + '%')).limit(limit)

    @classmethod
    def get_files_count(cls):
        return db.session.query(Instance).count()

    @staticmethod
    def check_file_exists(f_name):
        if Instance.query.filter_by(file_name=f_name).first() is None:
            return False
        return True

    @classmethod
    def remove_file(cls, _id):
        Instance.query.filter_by(id=_id).delete()
        db.session.commit()

    def remove_all_files():
        db.session.query(Instance).delete()
        db.session.commit()

    def create_inmem():
        conn = sqlite3.connect("file::memory:?cache=shared")
        cursor = conn.cursor()
        
        cursor.executescript('''
                            DROP TABLE IF EXISTS inmemory;   
                            CREATE TABLE IF NOT EXISTS inmemory(
                                id INTEGER PRIMARY KEY,
                                owner TEXT NOT NULL,
                                file_name TEXT NOT NULL,
                                file_type TEXT NOT NULL,
                                file_path TEXT NOT NULL,
                                file_size FLOAT NOT NULL,
                                file_size_unit TEXT NOT NULL,
                                last_modified TEXT);''')
        conn.close()

    def update_inmem(results_list, inst_name):
        conn = sqlite3.connect("file::memory:?cache=shared")
        cursor = conn.cursor()
        for res in results_list:
            query = '''
                    INSERT INTO inmemory (owner, file_name, file_type, file_path, file_size, file_size_unit, last_modified)
                    VALUES (?,?,?,?,?,?,?);'''
            cursor.execute(query, (inst_name, res['name'], res['type'],\
                         res['path'], res['size'], res['size unit'], res['last modified'],))
        conn.commit()
        conn.close()
        
    def query_inmem_for_files(file_n, limit):
        conn = sqlite3.connect("file::memory:?cache=shared")
        cursor = conn.cursor()
        files = []
        if limit is not None:
            cursor.execute('''SELECT * FROM inmemory WHERE file_name LIKE ? LIMIT ?''' ,
                             ('%'+file_n+'%',limit))
            files = cursor.fetchall()
        else:
            cursor.execute(f'''SELECT * FROM inmemory WHERE file_name LIKE "{file_n}%"''')
            files = cursor.fetchall()
        conn.close()
        return files

    def get_ip(request):
        """Getting the ip from the request of the calling instance.
        :return: instance ip.
        """
        split_request = request.host_url.split('/')
        second_split = split_request[2].split(':')
        inst_ip = second_split[0]
        return inst_ip

    def get_port(request):
        """Getting the port from the request of the calling instance.
        :return: instance port.
        """
        split_request = request.host_url.split('/')
        second_split = split_request[2].split(':')
        inst_port = second_split[1]
        return inst_port

    def json(self):
        return {"id" : self.id,
                "name" : self.file_name,
                "type" : self.file_type,
                "path" : self.file_path,
                "size" : self.file_size,
                "size unit": self.file_size_unit,
                "last modified": self.last_modified}

    def __repr__(self):
        return '<File id:%r name:%r path:%r type:%r size:%r>' % \
                (self.id ,self.file_name, self.file_path, self.file_type, self.file_size)
