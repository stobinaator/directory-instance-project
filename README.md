## Getting Started

To install the required dependencies just run `pip install -r requirements.txt` from the project's directory.

python3 app.py <name_of_db> <port on which you want it to listen>

e.g. `python3 app.py KIT1 5001`

This will start the Instance listening to the given Port, as well as creating a database object in the `dbs/instances` folder. If that is the first time running this command, two more databases are going to be created in the `dbs` folder (one for the users and one for the connections).

## Connection (Instance)
In order to connect an instance to the connections "register", you need to make a POST request.

URL: `ip:port/connections?instance_name=XXX` name given as a query parameter.

## File uploads (Instance)
Single file:

URL : `ip:port/instances/files/single` with an object: `"file_path" : "path/try_out.txt"`.

Batch of files:

URL : `ip:port/instances/files/batch` with an object: `"directory" : "path/<folder_name>"`.

## File search (User)
As an User you need a User-Token before you can search for files. You have to call the URL: `ip:port/authentications?user_name=XXX`. Name again as a query parameter
You will receive a 20 char key, which you will need to put it in the Headers.<br>
In order to start searching at all, you will need the IP and Port of the Instance where you what to search for a particular file. <br>

URL: `http://IP:Port/instances/files/<string:file_name>?search_remote=True&limit=X`.

Headers: <br>
`'User-Token' : <token_received>` <br>

Query Parameters: <br>
`'search_remote': True/False/ empty`<br>
`'limit' : <integer>` <br>

If 'search_remote' is NOT provided, the user will search only locally in the instance, the 'limit' query can serve as help. 

## Deletion of files
One file: `ip:port/instances/<int:id>`.

All files: `ip:port/instances/all`.
