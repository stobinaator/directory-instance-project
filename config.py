import os
import pathlib
import sys

path = os.path.join(pathlib.Path(__file__).parent.absolute(), "dbs/")

class BaseConfig(object):
    FLASK_ENV = 'development'
    DEBUG = False
    TESTING = False
    SECRET_KEY = b'\n\x15\x19\xbb\xb5\xa7\x9a;\xe3\xba:\xbf\x8fm$\xb3\x1c\xf9}\xed64\x018'
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{path}/instances/{sys.argv[1]}.db'
    # SQLALCHEMY_BIND = {'connection' : f'sqlite:///{path}/connection.db',
    #                     'auth' : f'sqlite:///{path}/auth.db'}
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    DEBUG = True

class TestingConfig(BaseConfig):
    TESTING = True

class ProductionConfig(BaseConfig):
    FLASK_ENV = 'production'