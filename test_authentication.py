import unittest
import requests
from auth import Auth 

class TestAuthentication(unittest.TestCase):
    API_URL = "http://127.0.0.1:5001"
    AUTH_GET_ALL_URL = f'{API_URL}/authentications/all'
    AUTH_POST_URL = f'{API_URL}/authentications'
    AUTH_DELETE_URL = f'{API_URL}/authentications'
    AUTH_GET_COUNT_URL = f'{API_URL}/count'

    # GET 
    def test_1_get_all_users(self):
        r = requests.get(TestAuthentication.AUTH_GET_ALL_URL)
        self.assertTrue(r.status_code, 404)
        self.assertTrue(r.content, 'No users found')

    # POST
    def test_2_post_user(self):
        params = {'user_name' : 'Stoyan'}
        r = requests.post(TestAuthentication.AUTH_POST_URL, params=params)
        self.assertTrue(r.status_code, 201)
        self.assertTrue(r.content, 'User added successfully')

    # GET again
    def test_3_get_new_added_user(self):
        r = requests.get(TestAuthentication.AUTH_GET_ALL_URL)
        self.assertTrue(r.status_code, 200)

    # DELETE
    def test_4_delete_user(self):
        user_id = 1
        r = requests.delete(TestAuthentication.AUTH_DELETE_URL + f'/{user_id}')
        self.assertTrue(r.status_code, 204)

    # GET again
    def test_5_get_all_users(self):
        r = requests.get(TestAuthentication.AUTH_GET_ALL_URL)
        self.assertTrue(r.status_code, 404)
        self.assertTrue(r.content, 'No users found')

    # POST 2 users
    def test_6_add_two_users(self):
        params = {'user_name' : 'Stoyan'}
        params2 = {'user_name' : 'Ilia'}
        
        r = requests.post(TestAuthentication.AUTH_POST_URL, params=params)
        self.assertTrue(r.status_code, 201)
        self.assertTrue(r.content, 'User added successfully')

        r2 = requests.post(TestAuthentication.AUTH_POST_URL, params=params2)
        self.assertTrue(r2.status_code, 201)
        self.assertTrue(r2.content, 'User added successfully')

    # GET count of users in db
    def test_7_check_user_count(self):
        r = requests.get(TestAuthentication.AUTH_GET_COUNT_URL)
        self.assertTrue(r.status_code, 200)
        self.assertTrue(r.content, 2)

    # DELETE multiple
    def test_8_delete_users(self):
        r = requests.delete(TestAuthentication.AUTH_GET_ALL_URL)
        self.assertTrue(r.status_code, 204)

    # GET again
    def test_9_get_all_users_and_count(self):
        r = requests.get(TestAuthentication.AUTH_GET_ALL_URL)
        self.assertTrue(r.status_code, 404)
        self.assertTrue(r.content, 'No users found')

        r2 = requests.get(TestAuthentication.AUTH_GET_COUNT_URL)
        self.assertTrue(r2.status_code, 200)
        self.assertTrue(r2.content, 0)

if __name__ == '__main__':
    unittest.main()