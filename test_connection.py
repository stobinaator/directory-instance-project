import unittest
import requests


class TestConnection(unittest.TestCase):
    API_URL = "http://127.0.0.1:5001"
    CONNS_ALL_URL = f'{API_URL}/connections/all'
    CONNS_POST_URL = f'{API_URL}/connections'
    CONNS_DELETE_URL = f'{API_URL}/connections'
    CONNS_GET_COUNT = f'{API_URL}/count'

    
    # GET
    def test_1_get_all_connections(self):
        r = requests.get(TestConnection.CONNS_ALL_URL)
        self.assertTrue(r.status_code, 404)
        self.assertTrue(r.content, 'No connections found')
    
    # POST
    def test_2_post_connection(self):
        params = {'instance_name' : 'KIT1'}
        r = requests.post(TestConnection.CONNS_POST_URL, params=params)
        self.assertTrue(r.status_code, 201)
        self.assertTrue(r.content, 'Connection added successfully')

    # GET again
    def test_3_get_new_added_connection(self):
        r = requests.get(TestConnection.CONNS_ALL_URL)
        self.assertTrue(r.status_code, 200)

    # DELETE
    def test_4_delete_connection(self):
        conn_id = 1
        r = requests.delete(TestConnection.CONNS_DELETE_URL + f'/{conn_id}')
        self.assertTrue(r.status_code, 204)
        
    # GET again
    def test_5_get_all_connections(self):
        r = requests.get(TestConnection.CONNS_ALL_URL)
        self.assertTrue(r.status_code, 404)
        self.assertTrue(r.content, 'No connections found')
    
    # GET DELETE GET
    def test_6_add_two_count_delete_count(self):
        params = {'instance_name' : 'KIT1'}
        params2 = {'instance_name' : 'KIT2'}
        
        r = requests.post(TestConnection.CONNS_POST_URL, params=params)
        self.assertTrue(r.status_code, 201)
        self.assertTrue(r.content, 'Connection added successfully') 

        r2 = requests.post(TestConnection.CONNS_POST_URL, params=params2)
        self.assertTrue(r2.status_code, 201)
        self.assertTrue(r2.content, 'Connection added successfully') 

        r3 = requests.get(TestConnection.CONNS_GET_COUNT)
        self.assertTrue(r3.status_code, 200)
        self.assertTrue(r3.content, 2)

        conn_id = 1
        r4 = requests.delete(TestConnection.CONNS_DELETE_URL + f'/{conn_id}')
        self.assertTrue(r4.status_code, 204)

        r5 = requests.get(TestConnection.CONNS_GET_COUNT)
        self.assertTrue(r5.status_code, 200)
        self.assertTrue(r5.content, 1)
        

if __name__ == '__main__':
    unittest.main()